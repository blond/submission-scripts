# BLonD-submission-scripts

A collection of scripts and utilities to set up and submit jobs (including scans) in htcondor.

## Installation

1. Download and unzip, or git clone the repository.
2. Install by running `pip install -e .` inside the project directory.
3. Now you can use the `get_interactive_job` command anywhere in your system. 

## Contents

- `job_management/` : Contains utility scripts for basic job management i.e. submit jobs, remove jobs, monitor runngin jobs, get status of completed jobs. 
- `prepare_runs.py` : Creates a separate simulation run directory for each input configuration. By default the directories will be placed inside `sim_runs`. A file with a list of the generated configurations (by default `sim_run_list.txt`) will be also created inside `sim_runs`.
- `executable.sh` : The executable script that will be run by every worker nodes. The script accepts an input parameter which is a yaml file with the simulation configuration. 
- `htcondor.sub` : The htcondor submission file that contains job resource requirements (memory/ cores/ etc) and will submit one job per input line in the `sim_runs/sim_run_list.txt` file. 
- `mainfile.py` : The simulation main file. Expects an input argument, which is the yaml configuration file. A set of parameters, defined in `prepare_runs.py` are read from the configuration file. 
- `interactive_jobs`: Use the `get_interactive_job.py` script to get interactive access to different types of computing nodes (CPU, CPU-AMD, GPU, GPU-T4, GPU-V100, GPU-A100)

## Step-by-step usage instructions

1. Define the parameter space that will be scanned in `prepare_runs.py`. 
    * The dictionary `global_params` contains parameter values that will be defined for every generated configuration. 
    * The dictionary `one_by_one` will combine element-wise the values of every defined parameter. 
    * The dictionary `all_by_all` will combine every parameter value with the values of all other parameters. 
    * Run the script. One directory (inside `sim_runs` by default) will be created for each configuration. All generated configurations will be listed in a file called, by default `sim_runs/sim_run_list.txt`.
2. Edit the variables in the beginning of `executable.sh` so that BLonD, python and the main file can be found by the worker nodes that will run the simulations. 
3. Edit the `htcondor.sub` submission file. 
    * Add your mail address and modify the notification variable to receive notifications for completed jobs. 
    * Use JobFlavour or MaxRuntime to define the maximum run time of a single simulation. 
    * Use reqeust_cpus to request for 1 or more number of CPU cores. 
    * By default 2GB of RAM will be allocated per CPU core, but this can be modified by the request_memory variable. 
    * More details can be found in the comments or here: https://htcondor.readthedocs.io/en/latest/man-pages/condor_submit.html
4. Place your mainfile in `mainfile.py`. 
    * Your main script should accept a string argument which is the name of the yaml file with the configuration parameters. 
    * Add the command `print('Done!')` in the end of your main script so that the `get_job_status.py` utility script will be able to detect that the job completed successfully. 
    * More details in `mainfile.py`. 
5.  Launch the parameter scan with: `./job_management/submit_all.py`
6. (Optional) You can monitor the submitted jobs with `./job_management/monitor_live.sh`
7. (Optional) You can delete all submitted jobs with `./job_management/remove_all.sh`
8. (Optional) You can get information about completed jobs with `./job_management/get_job_status.py`. 

## Interactive access

* The script `interactive_jobs/get_interactive_job.py` can be used to request for interactive access to a node. 
* The script accepts arguments for the type of device you request for, the time duration, the cpu cores and the amount of memory of the request. 
* Run it with the `--help` for more information. 
* After installing the package with `pip install -e .` , the `get_interactive_job` will be available from any location in your system. 
