#!/usr/bin/env python

import argparse
import os
import sys
import re
this_directory = os.path.dirname(os.path.realpath(__file__)) + "/"


parser = argparse.ArgumentParser(
    description='Detect failed jobs. ',
    usage='{} -i infile -o outfile'.format(sys.argv[0]))

parser.add_argument('-i', '--infile', type=str, default=f'{this_directory}/../sim_runs/sim_run_list.txt',
                    help='The file with the simulation runs list.')

parser.add_argument('-o', '--outfile', type=str, default=f'failed_run_list.txt',
                    help='The file to write any failed simulation runs.')

def main():
    args = parser.parse_args()
    sim_list_file = args.infile
    failed_sim_list_file = args.outfile

    assert os.path.exists(sim_list_file), f'File {sim_list_file} does not exist.'

    success = []
    fail = []
    missing = []
    for sim_dir in open(sim_list_file):
        sim_dir = sim_dir.strip()
        output_file = os.path.join('sim_runs', sim_dir, 'output.txt')
        # error_file = os.path.join('sim_runs', sim_dir, 'error.txt')
        # log_file = os.path.join('sim_runs', sim_dir, 'log.txt')
        if os.path.exists(output_file):
            # Keep only last 50 lines        
            lines = ''.join(open(output_file).readlines()[-50:])
            # print(lines)
            if re.search('Done!', lines):
                success.append(sim_dir)
                # print(f'Run: {sim_dir} completed succesfully!')
            else:
                # print(f'Run: {sim_dir} failed!')
                fail.append(sim_dir)
        else:
            missing.append(sim_dir)

    failed_sim_list = []
    if len(success):
        print('The following runs completed successfully:')
        for run in success:
            print(f'\t{run}')
        print()

    if len(fail):
        print('The following runs failed:')
        for run in fail:
            print(f'\t{run}')
        print()
        failed_sim_list += fail

    if len(missing):
        print('The outputs of the following runs are missing:')
        for run in missing:
            print(f'\t{run}')
        print()

    if len(failed_sim_list):
        with open(failed_sim_list_file, 'w') as outfile:
            outfile.writelines('\n'.join(failed_sim_list))

if __name__ == '__main__':
    main()